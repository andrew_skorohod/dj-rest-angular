
## Django Rest+ AngularJS app

This is simple CRUD application for working with  contacts.
Made with Django, Django rest framework, AgularJS.
Angular handles all frontend & makes request to API for all data.
DRF works as API.

The goal of making this app was to create one page application that uses REST and to practice AngularJS.
Also ,i think ,thati'm going to rewrite this app using NODE JS(MEAN stack)