from django.db import models
from django.core.validators import RegexValidator

# Create your models here.
class Contact(models.Model):
    createDate = models.DateTimeField(auto_now_add=True)
    firstName = models.CharField(max_length=255)
    lastName = models.CharField(max_length=255,blank=True,null=True)
    email = models.CharField(max_length=255,blank=True,null=True)
    phone_regex = RegexValidator(regex=r'^\+?1?\d{9,15}$',
                                 message="Phone number must be entered in the format: '+999999999'. Up to 15 digits allowed.")
    phone = models.CharField(max_length=255,validators=[phone_regex], blank=True,null=True)  # validators should be a list
    address = models.CharField(max_length=255,blank=True,null=True)


    class Meta:
        ordering = ('createDate',)

    def __str__(self):
        return self.firstName