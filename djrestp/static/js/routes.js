app.config(function($routeProvider) {
    $routeProvider
        .when("/", {
            templateUrl: "/static/templates/list.html",
            controller: "ListController",
            resolve: {
                contacts: function(Contacts) {
                    return Contacts.getContacts();
                }
            }
        })
        .when("/new/contact/", {
            controller: "NewContactController",
            templateUrl: "/static/templates/form.html"
        })
        .when("/contact/:contactId/", {
            controller: "EditContactController",
            templateUrl: "/static/templates/contact.html"
        })
        .otherwise({
            redirectTo: "/"
        })
})